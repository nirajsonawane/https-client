package com.example.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableFeignClients
public class ClientApplication {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	protected TestClient testClient;


	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}
	@Bean
	ApplicationRunner init(RestTemplate restTemplate) {
		return (ApplicationArguments args) ->  dataSetup(restTemplate);
	}

	private void dataSetup(RestTemplate restTemplate) {
		ResponseEntity<String> forEntity = restTemplate.getForEntity("https://localhost:8443/hello", String.class);
		System.out.println(forEntity.getStatusCode());
		System.out.println(forEntity.getBody());
		System.out.println("--------------------------- Client is Action ");
		String hello = testClient.getHello();
		System.out.println(hello);


	}


}
